﻿using System;
using Microsoft.EntityFrameworkCore;

using DockerAPI.Models;

namespace DockerAPI.Contexts
{
    public class ApplicationDbContext : DbContext
    {
        public DbSet<Actor> Actors { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> contextOptions)
            :base(contextOptions)
        {
            Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Actor>().HasData(new Actor()
            {
                Id = 1,
                Name = "Mathias Berntsen"
            });

            modelBuilder.Entity<Actor>().HasData(new Actor()
            {
                Id = 2,
                Name = "Nicolas Anderson"
            });

            modelBuilder.Entity<Actor>().HasData(new Actor()
            {
                Id = 3,
                Name = "Andre Ottosen"
            });

            modelBuilder.Entity<Actor>().HasData(new Actor()
            {
                Id = 4,
                Name = "Nicholas Lennox"
            });

            modelBuilder.Entity<Actor>().HasData(new Actor()
            {
                Id = 5,
                Name = "Dewald Els"
            });
        }
    }
}
