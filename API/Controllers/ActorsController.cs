﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using DockerAPI.Contexts;
using DockerAPI.Models;

namespace DockerAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ActorController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public ActorController(ApplicationDbContext context)
        {
            _context = context;
        }

        [HttpGet("test")]
        public string Test()
        {
            return "Yes, tested";
        }

        [HttpGet]
        public IEnumerable<Actor> Get()
        {
            return _context.Actors.ToList();
        }
    }
}
